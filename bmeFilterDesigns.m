
%% Filter Design Windowing.
%% 1.1
% The rectangular window has the narrowest possible mainlobe for a
% specified sidelobe level (window #1). Of all window, Chebyshev window
% displays the least largest sidelobe (window #2).Hann window, on the other
% hand, has the largest side lobe slope (60 dB/decade), which results in the least
% smallest sidelobe (window #3). The time domain signal is plotted on the
% left,and the log scale spectra of these three windows are plotted on the
% right of Figure 1.
clear
clc
N = 64;
w1 = rectwin(N);
w2 = chebwin(N);
w3 = hann(N);
wvt = wvtool(w1,w2,w3);
ch15 = chebwin(15);
ch64 = w2;
ch81 = chebwin(81);

%% 1.2
% Finite impulse-response (FIR) lowpass filters with cutoff 0.35? is
% designed using fir1 and the three Blackman-Harris windows from 1.1 of
% lengths 15, 64 and 81 samples. The time-domain response is plotted in
% figure 2, and the freuqency spectra are plotted in figure 3. This is a
% process of windowing. The time domain plot (figure 2) represents the
% FIR filter impulse response, and the bottom plot in frequency domain
% represents the amplitude response.

fir15 = fir1(14,0.35,'low',ch15);
fir64 = fir1(63, 0.35,'low', ch64);
fir81 = fir1(80, 0.35,'low', ch81);



figure (2)
plot(fir15)
hold on
plot(fir64,'r')
hold on
plot(fir81,'g')
xlabel('Time (sample)')
ylabel('Amplitude')
title('Figure 2. Time domain filter response')
xlim([0,81])
legend('Length 15','Length 64', 'Legnth 81')
hold off
hfvt = fvtool(fir15,1,fir64,1,fir81,1);
legend(hfvt,'Length 15','Length 64', 'Legnth 81')

%% 1.3
% The step responses of different filter legnths are scaled so that they
% are easy to compare. The filter length has to determined based on the
% required width of the transition band. As seen in Figure 4,the settling
% time is longer and the overshoots are bigger for a longer filter length.
% Moreover, the main lobe width is significantly smaller (figure 3) and the
% transition width is narower for the longer filter length (figure 4). In
% FIR filter, filter length is equivalent to the length of the impulse
% response, as seen in Figure 2. It is also observed in figure 2 that the
% width of the main lobe decreases as the filter length increases. When
% using the window that is not rectangular, side-lobes have to be lower to
% prevent abrupt discontinuity. The ripples are smaller in other windows,
% which is expected to achieve the ideal filter. However, the transition
% width is wider as a trade-off. This can be compensated by increasing the
% filter length, which can shorten the transition bandwidth as shown in
% Figure 4. Yet, one has to carefully choose the filter length for the
% different types of windows because as seen in Figure 2 and 4, the
% sidelobe peaks are higher as the filter length increases, which is not
% expected for ideal filter.

step15 = stepz(fir15);
step64=stepz(fir64);
step81=stepz(fir81);
length = 1:1:81;

newstep15 = nan(1,81);
newstep64 = nan(1,81);
for i= 1:size(step15)
    newstep15(i*5) = step15(i);
end

for j = 1:size(step64)
    newstep64(round(1.2*j))= step64(j);
end


figure (4)
stem(step81)
hold on
stem(newstep64,'r')
hold on
stem(newstep15, 'g')
xlabel('Time (samples)')
ylabel('Amplitude')
title('Figure 4. Step responses of Windw-based filter')
legend('Length 81','Length 64', 'Legnth 15')

%% Problem 2
%% 2.1
f = [0 0.35 0.5 1];         % Frequency band edges
a = [1  1  0 0];           % Desired amplitudes
pm15 = firpm(14,f,a);
pm64 = firpm(63, f,a);
pm81 = firpm(81, f, a);

figure (5)
plot(pm15)
hold on
plot(pm64,'r')
hold on
plot(pm81,'g')
xlabel('Time (sample)')
ylabel('Amplitude')
title('Figure 5. Time domain filter response')
xlim([0,81])
legend('Length 15','Length 64', 'Legnth 81')
hold off
pmvt = fvtool(pm15,1,pm64,1,pm81,1);
legend(pmvt,'Length 15','Length 64', 'Legnth 81')

%% 2.2
% As similar to what is done in Problem 1.3, the step responses are
% rescaled. The major difference between the Chebyschev window-based filter
% and Parks-McClellan filter is that the side lobe peaks of equiripple
% filter are equal in the time-domain filter response (Figure 5). In
% addition, the main lobe width has not significantly changed regardless of
% the filter length (figure 6), when compared to figure 3. While ripples at
% of the step response is smaller for shorter filter legnth in figure 4,
% the ripples in figure 6 does not show much change in response to the
% difference in filter length. This observation can be attributed to the
% fact that there is not much side lobe changes, as seen in figure 5.

pmstep15 = stepz(pm15);
pmstep64=stepz(pm64);
pmstep81=stepz(pm81);

newpmstep15 = nan(1,81);
newpmstep64 = nan(1,81);
for i= 1:size(step15)
    newpmstep15(i*5) = pmstep15(i);
end

for j = 1:size(step64)
    newpmstep64(round(1.2*j))= pmstep64(j);
end

figure (6)
stem(pmstep81)
hold on
stem(newpmstep64,'r')
hold on
stem(newpmstep15, 'g')
xlabel('Time (samples)')
ylabel('Amplitude')
title('Figure 6. Step responses of Parks-McClellan filter')
legend('Length 81','Length 64', 'Legnth 15')


%% 3.1
% The magnitude repsonse spectra of both filters(Chebyschv window-based and
% equiripple filter) are plotted in the figure below. Exceptfor the first
% few side lobe of the fir1 filter, the filter looks similar to equiripple
% filter. The transition bandwith of remez filter is slightly shorter, but
% the width of the main lobe is wider. The magnitudes of the sidelobes of
% remez filter are higher. Yet, in general, two filters display similar
% characteristics.
fmpm = fvtool(fir81,1,pm81,1);
legend(fmpm,'fir1','remez')

%% 3.2
% The EMG signals are filtered with the filters above. However, as shown in
% figure 9 and 10, these filters are not approriate for separating two
% signals.
clc
Fs = 1000;
load('Project4Data.mat');
y = emg;
t = (0:size(y)-1)/Fs;

out2 = filter(pm81,1,y);
figure (9)
subplot(2,1,1)
plot(t,y)
ylabel('Amplitude')
xlabel('Time (s)')
title('Original Signal')
subplot(2,1,2)
plot(t,out2)
title('Parks-McClellan Filtered Signal')
xlabel('Time (s)')

figure (10)
out1 = filter(fir81,1,y);
subplot(2,1,1)
plot(t,y)
ylabel('Amplitude')
xlabel('Time (s)')
title('Original Signal')
subplot(2,1,2)
plot(t,out1)
title('Chebyschev-Based Filtered Signal')
xlabel('Time (s)')

%% 3.2
% One can extract the breathing EMG signal by using the butterworth signal
% using the specificatoins below. The filtered signal is plotted in Figure
% 11.
ord = 80;

low = 0.4;
bnd = [0.6 0.9];
rectTest = fir1(4, 0.006, 'low',flattopwin(5),1,y);

Fc=3; % Cut-off frequency (from 2 Hz to 6 Hz depending to the type of your electrod)
N=4; % Filter Order
[B, A] = butter(N,Fc*2/Fs, 'low'); %filter's parameters

butterEMG=filter(B,A, y); %in the case of real-time treatment

fvtool(butter(N,Fc*2/Fs, 'low'),1)

figure (11)
subplot(2,1,1)
plot(y)
ylabel('Amplitude')
xlabel('Time (s)')
title('Original Signal')
subplot(2,1,2)
plot(butterEMG)
ylabel('Amplitude')
xlabel('Time (s)')
%% 3.2 Continued
% The attempt to separate the other signal was done by using one low pass
% with Fc of 5Hz, high pass of 0.5Hz and notch filter at 2.5 Hz. The
% filtered signal is plotted below:
clear;
clc;
load('Project4Data.mat');
Fs = 1000;
y = emg;
t = (0:size(y)-1)/Fs;


fc1 = 5;
Wn = (2/Fs)*fc1;
kaiserLow = fir1(100,Wn,'low',kaiser(101,3));

fc2 = 0.5;
kaiserHigh = fir1(100,2/Fs*fc2, 'high', kaiser(101,3));

b = fir1(100,[0.1 0.10001]);

d  = fdesign.notch('N,F0,Q,Ap',6,0.005,10,1);
    Hd = design(d);


hh = fvtool(kaiserHigh,1, kaiserLow,1,Hd,1,'Fs', Fs);
legend(hh,'HighPass','LowPass', 'Notch filter')
testEMG = filter(Hd,(filter(kaiserHigh, 1, filter(kaiserLow,1,y))));

figure (12)
subplot(2,1,1)
plot(t,y)
ylabel('Amplitude')
xlabel('Time (s)')
title('Original Signal')
subplot(2,1,2)
plot(t,testEMG)
title('Cascade-Filtered Signal')
ylabel('Amplitude')
xlabel('Time (s)')

%%
% Another filter was tested:
fvtool(taylorwin(81))
testtest = filter(taylorwin(81),1,y);
figure (13)
subplot(2,1,1)
plot(t,y)
ylabel('Amplitude')
xlabel('Time (s)')
title('Original Signal')
subplot(2,1,2)
plot(t,testEMG)
title('Filtered Signal Directly with taylorwin')
ylabel('Amplitude')
xlabel('Time (s)')